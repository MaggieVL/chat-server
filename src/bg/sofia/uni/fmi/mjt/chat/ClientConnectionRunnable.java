package bg.sofia.uni.fmi.mjt.chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Set;

public class ClientConnectionRunnable implements Runnable {

    private String username;
    private Socket socket;

    public ClientConnectionRunnable(String username, Socket socket) {
        this.username = username;
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            while (true) {
                String commandInput = reader.readLine();

                if (commandInput != null) {
                    String[] tokens = commandInput.split("\\s+");
                    String command = tokens[0];
                    if ("send".equals(command)) {
                        String to = tokens[1];
                        String message = commandInput.substring(command.length() + to.length() + 2);

                        Socket toSocket = ChatServer.getUser(to);
                        if (toSocket == null) {
                            writer.println(String.format("=> %s seems to be offline", to));
                        } else {
                            sendMessage(to, message);
                        }
                    } else if ("list-users".equals(command)) {
                        Set<String> users = ChatServer.getUsernameSet();

                        if (users.size() <= 1) {
                            writer.println("nobody is online");
                        } else {
                            for (String user : users) {
                                writer.println(user);
                            }
                        }
                    } else if ("send-all".equals(command)) {
                        Set<String> users = ChatServer.getUsernameSet();
                        users.remove(username);
                        
                        if (users.isEmpty()) {
                            writer.println("There are no users online to send the message to.");
                        }

                        String message = commandInput.substring(command.length() + 1);

                        for (String receiver : users) {
                            sendMessage(receiver, message);
                        }
                    } else if("disconnect".equals(command)) {
                      ChatServer.removeUser(username);  
                     // this.socket.close();
                      break;
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("socket is closed");
            System.out.println(e.getMessage());
        }
    }

    private void sendMessage(String receiver, String message) throws IOException {
        System.out.println("inside");
        PrintWriter toWriter = new PrintWriter(ChatServer.getUser(receiver).getOutputStream(), true);
        toWriter.println(String.format("[%s]: %s", username, message));
        System.out.println("successfully sent message");
    }

}
